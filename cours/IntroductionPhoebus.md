# Introduction

**Phoebus** est la version actuelle de l'outil CSS (Control System Studio).
<br>
Le site officiel est ici :https://controlssoftware.sns.ornl.gov/css_phoebus/.
<br>
Cet outil permet de développer des **IHM (interface homme machine)** en proposant une bibliothèque de **widgets graphiques** dédiées aux accélérateurs de particules.
<br>
C'est un outil open-source (donc gratuit) qui peut fonctionner sur windows, linux ou macosx.
<br>
Il s'interface notamment avec les systèmes **EPICS**.
<br>
La documentation se trouve ici : https://control-system-studio.readthedocs.io/en/latest/


# Vocabulaire

**IHM** : Interface Homme Machine
<br>
**widget** : composant graphique d'une IHM (un bouton, un graphique...)

<br>

# Quelques exemples d'IHM développées au GANIL (situé Caen) sur l'installation SPIRAL2

## les cavités HF
![Alt text](metierHf.png)

## la source d'ions
![Alt text](sourceLbe1.png)

## le synoptique SPIRAL2

![Alt text](synoptiqueSpiral2.png)


# Quelques exemples de widgets

- Une bonne façon de se faire une idée des widgets disponibles dans Phoebus est d'ouvrir les `example display`.

![Alt text](exampleDisplay.png)

- En cliquant sur widget, vous pouvez voir quelle utilisation vous pourriez en faire. Ci-dessous un exemple de byte monitor.

![Alt text](byteMonitorWidget.png)

- Pour pouvoir visualiser ce qui se cache derrière un widget d'exemple, il faut installer les ExampleDisplay.

![Alt text](installExampleDisplay.png)

## Graphics

Permet de dessiner des objets graphiques (rectangles, cercles, ellipses, polygones)

## Monitors

Cette catégorie de widgets permet de visualiser des valeurs sur des équipements de différentes manières.
- vue LED
- vue texte
- vue sous forme de progress bar
- vue sous forme de tableau
- vue thermomètre

Chaque widget peut être associé à une PV EPICS.

## Controls

Cette catégorie de widgets permet d'effectuer des actions.
- boutons cliquables
- check box
- boutons de choix
- sélecteur de fichier
- saisie de texte

Chaque widget peut être associé à une PV EPICS.

## Plots

Cette catégorie permet de créer des graphiques.
- Data Browser => visualiser l'évolution des valeurs d'une PV
- X/Y Plot => graphique pour visualiser un graphique sous forme de courbe, barre graph ...

## Structure

Cette catégorie va servir à structurer votre IHM.
- grouper des widgets
- Créer des onglets (tab) dans votre IHM

## Miscellaneous

On trouve ici :
- les vues 3D
- les web browser

## Solutions web sous EPICS
Il existe plusieurs solutions web, la suite ne se veut pas être exhaustive.
### Display Builder Web Runtime (DBWR)
Quand on utilise Phoebus avec des vues de type bob, l'équipe de développement fournit une solution qui permet d'afficher les mêmes vues via un navigateur web. Le logiciel est DBRW (Display Builder Web Runtime): https://github.com/ornl-epics/dbwr  

![Alt text](architectureDbwr.png)

DBRW est la partie frontend du logiciel, une fois que l'interface web est chargé chez le client, le serveur PVWS (PV Web Socket) est en charge d'envoyer les informations au client d'un côté et de l'autre de la communication EPICS en Channel Access (CA) ou PV Access (PVA) : https://github.com/ornl-epics/pvws  

![Alt text](dbwrScreenshot.png)

Ces logiciels sont développés en Java, Javascript et HTML, le code est sous forme de servlet et tourne dans un serveur Tomcat. **L'interface web est une interface de visualisation, on ne peut pas l'utiliser pour contrôler à distance (lecture seule).**
Depuis un navigateur web, on peut atteindre les 2 à travers des URL telles que :
- http://localhost:8080/pvws
- http://localhost:8080/dbwr


### Grafana
Grafana est un outil open source pour visualiser les données sous forme de tableau de bord et de courbe dans un navigateur web. Grafana est multiplateforme, il s'appuie sur un stockage dans une base de données. Grafana permet aux utilisateurs de visualiser les données de l'archivage Archive Appliance, mais aussi de définir des alarmes.
Il existe 2 solutions pour connecter Grafana à Archive Appliance :
- https://github.com/sasaki77/archiverappliance-datasource
- https://github.com/KeckObservatory/epics-grafana-datasource
Grafana est fait pour être facile à utiliser. Les alertes définies dans Grafana n'ont pas d'influence sur le partie EPICS.  

![Alt text](grafanaScreenshot.png)

Les alertes peuvent être via email, Kafka, Slack, ...


### REACT AUTOMATION STUDIO
Cette solution est basée sur la technologie REACT : https://github.com/React-Automation-Studio/React-Automation-Studio.
Cette solution permet de piloter une expérience et de définir des alarmes. Les alarmes peuvent être acquittées, on peut les rechercher à travers un filtre. Les canaux d'alertes peuvent être Email, SMS, Signal, WhatsApp. Un aspect intéressant est la configuration de niveau de droit directement configurable depuis l'interface web. Les vues synoptiques se font par codage, il n'y a pas de widget avec des drag & drop.

![Alt text](ReactAutomationbeamlineStudioScreenshot.png)  


### Web Interface for Controls Applications (WICA-HTTP)
Cette solution est développée par PSI. Elle est composée de 2 parties :
- Wica-HTTP : Backend HTTP serveur qui reçoit les requêtes des clients et génère un flux de données
- Wica-JS : Frontend Javascript
Cette solution est compatible Channel Access uniquement.
Pour lire une PV, on va utiliser l'Url "http://localhost:8080/ca/channel/pvName". On peut l'utiliser pour contrôler à distance (lecture/écriture).


### Autres solutions
Autres solutions pour des synoptiques
- WEBPV de ESS : Web socket entre client et serveur
- J-PARC : Prend des snapshots d'OPIs (png), compatible avec la sécurité

Autres solutions pour visualisation des valeurs et débugue :
- DESY : Compatible avec plusieurs sources de données (live ou archivage), affichage de courbe
- Canadian light source : Affichage de liste de PV, d'IOCs, clients
- LNBL : Liste de PVs et IOCs
- ISIS : Basé sur "Archive engine" pour afficher des valeurs  


# References
 - Display Builder Web Runtime et PV Web Socket : https://controlssoftware.sns.ornl.gov/training/2022_USPAS/Presentations/06%20Display%20Web%20Runtime.pdf
- Grafana : https://indico.fhi-berlin.mpg.de/event/52/contributions/577/attachments/213/661/ArchiverAppliance_EPICS_Fall_2020.pdf, https://indico.lightsource.ca/event/2/contributions/62/attachments/46/89/Day%204%20-User%20created%20Alerts%20in%20Grafana%20.mp4
- REACT AUTOMATION STUDIO : https://indico.lightsource.ca/event/2/contributions/29/attachments/43/119/Presentation%20-%20REACT%20Automation.pdf, https://www.youtube.com/watch?v=FBnG7m5QevE 
- WEBPV et Public Operations Screen: https://indico.cern.ch/event/766611/sessions/304575/attachments/1854878/3046213/kocevar_-_EPICS_Web_Services_at_ESS.pdf
- Comparatif solutions web : https://indico.cern.ch/event/766611/contributions/3438480/attachments/1855753/3047989/LAE-WEBSERVICESREPORT.pdf

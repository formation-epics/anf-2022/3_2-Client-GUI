- [Informations](#informations)
  - [Objectifs](#objectifs)
  - [Prérequis](#prérequis)
  - [Déroulement](#déroulement)
- [Créer une IHM simple](#créer-une-ihm-simple)
  - [démarrage de l'outil Phoebus](#démarrage-de-loutil-phoebus)
  - [Créer un display](#créer-un-display)
  - [Visualiser la valeur d'une PV EPICS (widgets de type Monitors)](#visualiser-la-valeur-dune-pv-epics-widgets-de-type-monitors)
    - [Exercice 1](#exercice-1)
    - [Exercice 2](#exercice-2)
    - [Exercice 3](#exercice-3)
  - [Changer la valeur d'une PV EPICS (widget de type Controls)](#changer-la-valeur-dune-pv-epics-widget-de-type-controls)
    - [Exercice 1](#exercice-1-1)
    - [Exercice 2](#exercice-2-1)
  - [Déclencher des actions](#déclencher-des-actions)
    - [Exercice 1 : créer des boutons qui ouvrent et ferment une vanne](#exercice-1--créer-des-boutons-qui-ouvrent-et-ferment-une-vanne)
    - [Exercice 2 : effectuer une action avec un script python](#exercice-2--effectuer-une-action-avec-un-script-python)
  - [Visualiser des valeurs sous forme de tableau](#visualiser-des-valeurs-sous-forme-de-tableau)
    - [Exercice 1](#exercice-1-2)
  - [Créer des graphiques](#créer-des-graphiques)
    - [Exercice 1 : graphique de type X/Y Plot](#exercice-1--graphique-de-type-xy-plot)
    - [Exercice 2 : tracer une courbe avec le X/Y Plot](#exercice-2--tracer-une-courbe-avec-le-xy-plot)
  - [Modifier les propriétés d'un widget](#modifier-les-propriétés-dun-widget)
    - [Exercice](#exercice)

# Informations

## Objectifs

Créer une IHM avec l'outil phoebus permettant de :
- visualiser des valeurs sur des équipements gérés par un IOC EPICS 
- envoyer des commandes sur des équipements gérés par un IOC EPICS.

Durée : 90 min

La correction est dans `TP/ihm/`. Vous y trouverez le fichier `main.bob` qui est le fichier phoebus pour l'IHM et un répertoire `scripts` qui contient les scripts `python`.

## Prérequis
- connaissances sur les IOCs EPICS
- quelques notions en language python

## Déroulement
- Création de différents widgets
- Interfacer ces widgets avec des PVs EPICS
- Créer des scripts python se déclenchant sur des actions faites sur des widgets (clics de bouton ...)

# Créer une IHM simple

## démarrage de l'outil Phoebus

Pour démarrer l'outil, utiliser la commande `phoebus` dans un terminal linux.

```shell
[epicslearner@rocky ~]$ phoebus 
[epicslearner@rocky tmp]$ 2022-09-16 14:38:39 CONFIG [org.phoebus.product.Launcher] Loading settings from /opt/epics/extensions-formation/phoebus-4.6.6/settings.ini
2022-09-16 14:38:39 INFO [org.phoebus.product.Launcher] Phoebus (PID 5033)
2022-09-16 14:38:40 WARNING [javafx] Unsupported JavaFX configuration: classes were loaded from 'unnamed module @199f1c31'
2022-09-16 14:38:41 INFO [org.phoebus.channelfinder.ChannelFinderClient] Creating a channelfinder client to : http://localhost:8080/ChannelFinder
2022-09-16 14:38:41 SEVERE [org.phoebus.framework.preferences] Reading Preferences: Java system property or Environment variable'$(arch)' is not defined
2022-09-16 14:38:42 WARNING [org.phoebus.logbook] Cannot locate logbook factory 'inmemory'

(java:5033): Gdk-WARNING **: 14:38:44.744: XSetErrorHandler() called with a GDK error trap pushed. Don't do that.
2022-09-16 14:38:48 WARNING [org.csstudio.display.builder.runtime] Jython will be unable to access scripts in examples:/scripts. Install examples in file system.
```
Vous devriez avoir la fenêtre qui s'ouvre.
![Alt text](startPhoebus.png)

## Créer un display

- Tous les widgets sont contenus dans un display. Pour créer un display, utiliser le menu suivant :

![Alt text](createDisplay.png)

- Les différents widgets disponibles sont sur le côté droit.
- Pour chaque widget de votre display, on peut accéder à ses propriétés. Il suffit de cliquer sur le widget et ses propriétés s'affichent sur le côté droit.

![Alt text](listWidgets.png)

## Visualiser la valeur d'une PV EPICS (widgets de type Monitors)

### Exercice 1
- Créer un display qu'on appellera `main.bob` dans le répertoire `3_2-Client-GUI/TP/ihm/`. Sa taille sera de 1600 x 800 pixels.
- Dans votre display `main.bob`, créer un widget de type `Text update` qui permet de visualiser la valeur de la PV `epicslearner:ValCourant`. Les caractéristiques du widget sont les suivantes :
  - Nom du widget = `TextUpdateValCourant`. 
  - positions x=100 y=50 
  - taille 200 x 50 
  - unités et précision : venant de la PV EPICS. 
  - largeur du bord = 2 pixels et de couleur noire
  - couleur de fond = gris clair (`Read Background`)
  - police = `Regular` de taille `20`
  - centrage horizontal et vertical

### Exercice 2
- Créer un widget de type `tank` qui permet de visualiser la valeur de la PV `epicslearner:ValCourant`. Les caractéristiques sont les suivantes :
  - Nom du widget = `TankValCourant`. 
  - position X = 100
  - position Y = 20
  - largeur = 200
  - hauteur = 400
  - couleur de remplissage : jaune
  - unités : venant de la PV EPICS
  - précision : venant de la PV EPICS
  - minimum : 0
  - maximum : 1000


- Démarrer votre IOC sur lequel tourne la PV EPICS epicslearner:ValCourant.
- Démarrer votre display `Main` en cliquant sur le bouton `play` en haut à droite.
- Changer les valeurs de la PV `epicslearner:ValCourant` à l'aide des commandes caput dans un terminal linux et visualiser les changements sur voter IHM.

### Exercice 3
- Créer un widget `LED` avec les caractéristiques sont les suivantes :
  - nom = `LedStatusVanne`
  - PV à visualiser = `epicsleaner:StatusVanne`
  - couleur = vert clair (avec affichage `ouvert`) quand la PV `epicsleaner:StatusVanne` est à 0 (vanne ouverte)
  - couleur = vert foncé (avec affichage `fermé`) quand elle est à 1 (vanne fermée).
  - position X = 400
  - position Y = 50
  - largeur = 50
  - hauteur = 50

## Changer la valeur d'une PV EPICS (widget de type Controls)

### Exercice 1
- Créer un widget de type `Text Entry` pour pouvoir changer la valeur de la PV `epicslearner:ConsCourant`. Les caractéristiques sont les suivantes :
  - position X = 600
  - position Y = 50
  - largeur = 200
  - hauteur = 50
  - couleur de fond = bleu turquoise (`Write Background`)
  - largeur du bord = 2 pixels et de couleur noire
  - centrage horizontal et vertical
- Lancer votre display et changer la valeur de votre widget.
- Dans un terminal linux, visualiser votre PV `epicslearner:ConsCourant` avec la commande `camonitor` et observer les changements de valeur.

### Exercice 2
- Créer un widget de type `Scaled Slider` pour pouvoir changer la valeur de la PV `epicslearner:ConsCourant`. Les caractéristiques sont les suivantes :
  - position X = 600
  - position Y = 120
  - largeur = 300
  - hauteur = 50
  - Lancer votre display et changer la valeur avec le slider.
- Dans un terminal linux, visualiser votre PV `epicslearner:ConsCourant` avec la commande `camonitor` et observer les changements de valeur.

## Déclencher des actions

### Exercice 1 : créer des boutons qui ouvrent et ferment une vanne

- Créer un bouton de type `Action Button` avec les caractéristiques suivantes :
  - nom = `ActionButtonOuvrirVanne`
  - PV = `epicslearner:CmdOuvrirVanne`
  - Text = `ouvrir vanne`
  - position x = 400 y = 120
  - taille 100 x 50
  - Actions = `writePv` afin d'écrire 1 sur la PV `epicslearner:CmdOuvrirVanne`
- Créer un autre bouton en dessous qui permet de fermer la vanne (utiliser la PV `epicslearner:CmdFermerVanne`)

### Exercice 2 : effectuer une action avec un script python

Dans cet exercice, nous allons utiliser les PVs dites locales de phoebus. Ce sont des PVs totalement virtuelles qui peuvent servir dans des cas bien particuliers.
- Créer un widget `TextEntry` qu'on nommera `TextEntryNumber1` et qu'on associera à la PV locale `loc://number1<VDouble>(0.0)`.

NOTE : pour déclarer une PV locale on utilise le mot clé `loc://` suivi du nom de la variable puis son type entre `< >` et sa valeur d'initialisation entre parenthèses.

- Créer 3 widgets analogues  `TextEntryNumber2`,  `TextEntryNumber3` et  `TextEntryNumber4`.
- Créer un bouton de type `Action Button` qu'on appellera `ActionButtonCalculMoyenne`. On l'associera à la PV `loc://calculerMoyenner<VString>("stop")`.
- Créer un widget de type `TextUpdate` qu'on appellera `TextUpdateMoyenne` associé à la PV `loc://moyenneVDouble>(0.0)`.

Au final, vous devriez obtenir ceci :

![Alt text](displayCalculMoyenne.png)

- Nous voudrions déclencher un script python qui va calculer la moyenne des 4 valeurs entrées dans chacun des widgets `TextEntryNumber`. Pour cela, il faut d'abord créer le fichier du script python. Ce fichier s'appellera calculerMoyenne.py et sera placé dans `/home/epicslearner/git/ANF_2022/3_2-Client-GUI/TP/ihm/scripts`. Ensuite, il faut cliquer sur le bouton `Scripts` dans les properties du widget `ActionButtonCalculMoyenne` et remplir la fenêtre comme ci-dessous.

![Alt text](createPythonScript.png)

- Sur la gauche, on a le nom du script.
- Sur la droite, on a toutes les PVs qui sont utilisées par le script. Ici, on utilise 6 PVs numérotés de 0 à 5.
  - en 0, on a la PV défini dans le champ `PV name` du widget `ActionButtonCalculMoyenne`.
  - de 1 à 4, on a toutes les PVs qui sont associées aux widgets `TextEntryNumber1` à `TextEntryNumber4`.
  - en 5, on a a mis la PV qui contiendra le résultat du calcul de la moyenne. C'est la PV qui est associée au widget `TextUpdateMoyenne`.


___
NOTE : L'aide phoebus pour la gestion des scripts python se situe ici : https://control-system-studio.readthedocs.io/en/latest/app/display/editor/doc/scripts.html?highlight=PVUtil#scripts

- Pour récupérer la valeur d'une PV dans un script python, on va utiliser la syntaxe suivante :
```shell
PVUtil.getString(pvs[0]) pour récupérer la valeur de la chaine de caractère contenue dans la PV situé à l'index 0.
PVUtil.getDouble(pvs[2]) pour récupérer la valeur flottante (double) de la PV situé à l'index 2. 
```
- Pour écrire la valeur d'une PV dans un script python, on va utiliser la syntaxe suivante :
```shell
pvs[5].write(2.2) pour écrire la valeur 2.2 dans la PV situé à l'index 5.
```
___

Le script python doit ressembler à ceci mais il manque des choses à compléter:


```python
# Description : Permet de calculer la moyenne de 4 valeurs et d'envoyer la moyenne dans un widget de type Text Update.
# Auteur : Olivier DELAHAYE 
# Date : 23/09/2022

# import des packages pour gérer les PVs et widgets dans phoebus
from org.csstudio.display.builder.runtime.script import PVUtil
from org.csstudio.display.builder.runtime.script import ScriptUtil
from org.csstudio.display.builder.runtime.script import ColorFontUtil

###############################################################################
# Le script commmence ici
################################################################################
print("> calculerMoyenne.py")

startStop = PVUtil.getString(pvs[0])
print("   pv0="+str(pvs[0]))
print("   startStop=" + str(startStop))
if startStop == "start":
    print("   demarrer calcul moyenne")
    val1 = PVUtil.getDouble(pvs[1])
    val2 = PVUtil.getDouble(pvs[2])
    ...

    pvs[5].write(moy)
    startStop = "stop"
print("< calculerMoyenne.py")


```

## Visualiser des valeurs sous forme de tableau

### Exercice 1
- créer un tableau (widget de type monitor) avec les caractéristiques suivantes :
  - 2 colonnes
  - la première colonne s'appelle `nombre`
  - la seconde colonne s'apelle `valeur`
  - largeur x hauteur = 200 x 200
  - largeur de colonne = 100 pour chaque colonne
- Créer un script python `tableauUpdate.py` dans `/home/epicslearner/git/ANF_2022/3_2-Client-GUI/TP/ihm/scripts`. Ce script ressemble à cela :
```python
# Description : Permet de mettre à jour un graphique
# Auteur : Olivier DELAHAYE 
# Date : 23/09/2022

# import des packages pour gérer les PVs et widgets dans phoebus
from org.csstudio.display.builder.runtime.script import PVUtil
from org.csstudio.display.builder.runtime.script import ScriptUtil
from org.csstudio.display.builder.runtime.script import ColorFontUtil

###############################################################################
# Le script commmence ici
################################################################################

print("> tableauUpdate.py")

tableData = []
for i in range(0, 4):   
    col0 = str(i+1)
    value = PVUtil.getDouble(pvs[i])
    print("    value = " + str(value))
    col1 = str(value)
    row = [col0, col1]
    tableData.append(row)
    print("   tableData=" + str(tableData))

widget.setValue(tableData)

print("< tableauUpdate.py")
```

Explication du script :
- il lit les valeurs des PVs 0, 1, 2 et 3 qu'on apelle val0, val1, val2, val3.
- il écrit les données de chaque ligne du tableau (avec `tableData.append(row)`)
- il met à jour le widget tableau avec la méthode `widget.setValue(tableData)`

- Activer ce script en l'enregistrant dans votre widget comme ci-dessous.
![Alt text](scriptPythonTableau.png)

**NOTE :** 
- On voit ici que les 4 PVs d'entrée sont celles qui contiennent les nombres qu'on a défini précédemment.
- On voit également qu' on a coché `Trigger` pour chaque PV ce qui signifie qu'à chaque fois que la valeur d'une des PVs va changer, le script sera déclenché.

## Créer des graphiques

### Exercice 1 : graphique de type X/Y Plot

Le graphique X/Y Plot est destiné à tracer des pts de coordonnées (x, y) où x et y sont des valeurs contenues dans des PVs.

![Alt Text](grapheValeurs01.png)


- Créer un widget de type `X/Y Plot` avec les caractéristiques suivantes :
  - name = grapheValeurs
  - title = graphe des valeurs
  - axe X allant de 0 à 5
  - 1 axe Y allant de 0 à 100
  - 4 traces car on veut tracer la valeur des 4 PVs loc://number1, loc://number2, loc://number3 et loc://number4
    - axe 1 : le champ `X PV` doit être rempli avec `loc://x1<VDouble>(1)` et le champ `Y PV` avec `loc://number1`
    - ...
    - axe 4 : le champ `X PV` doit être rempli avec `loc://x4<VDouble>(4)` et le champ `Y PV` avec `loc://number4`
  - Chaque trace doit être configuré pour afficher des `Bars` avec une largeur de ligne de 10 pixels.

A l'éxécution, vous devriez obtenir ceci :

![Alt Text](grapheValeurs02.png)


### Exercice 2 : tracer une courbe avec le X/Y Plot
L'objectif ici est de tracer une courbe qui représente l'évolution des valeurs contenues dans les PVs `loc://number1`, `loc://number2`, `loc://number3`, `loc://number4`.

On voudrait obtenir ce rendu (cf la courbe en jaune).

![Alt Text](grapheValeurs03.png)


Pour cela, il faut rajouter une 5ème trace et remplir les champs `X PV` doit être rempli avec `loc://tableauValeursX<VDoubleArray>(1,2,3,4)` et le champ `Y PV` avec `loc://tableauValeursY<VDoubleArray>(0,0,0,0)`.
  
L'idée est d'utiliser un script python qui va s'exécuter au lancement du graphe et qui va mettre à jour la variable locale `loc://tableauValeursY<VDoubleArray>` (cette variable est un tableau de 4 valeurs de type double) avec les valeurs des PVs `loc://number1`, `loc://number2`, `loc://number3`, `loc://number4`. Je vous laisse y réfléchir. On peut par exemple créer un script python nommé `graphUpdate.py` et le placer dans `/home/epicslearner/git/ANF_2022/3_2-Client-GUI/TP/ihm/scripts`.

## Modifier les propriétés d'un widget

Tous les widgets phoebus ont un nom et des propriétés (background color, font ...). Il est possible de changer dynamiquement via un script python chacune des ces propriétés.

### Exercice

- Dessiner un rectangle avec une propriété `background color` qui est à mise à `OFF` (vert foncé)
- Ajouter un bouton qui va déclencher un script python permettant de changer la couleur de fond du rectangle. 

**NOTE :**

- Pour récupérér l'objet widget à partir de son nom dans le script, il faut utiliser la fonction python `ScriptUtil.findWidgetByName` :

```python
# import du package ScriptUtil
from org.csstudio.display.builder.runtime.script import ScriptUtil

# récupération du widget à partir de son nom
mywidget = ScriptUtil.findWidgetByName(widget, 'Rectangle')
```

- Pour changer la propriété d'un widget, on utilise la fonction python `setPropertyValue` :

```python
# import du package ScriptUtil
from org.csstudio.display.builder.runtime.script import ScriptUtil
# import du package ColorFontUtil
from org.csstudio.display.builder.runtime.script import ColorFontUtil

# on change la propriété background_color du widget
mywidget.setPropertyValue("background_color", ColorFontUtil.RED)
```

- Tous les noms de propriété peuvent être trouvé facilement en modifiant la propriété dans l'interface phoebus et en allant visualiser votre fichier `.bob` à l'aide d'un éditeur de texte. Ci-dessous, j'ai mis un extrait du fichier `main.bob` pour le widget `rectangle`:

```XML
  <widget type="rectangle" version="2.0.0">
    <name>Rectangle</name>
    <x>600</x>
    <y>200</y>
    <width>110</width>
    <height>60</height>
    <background_color>
      <color name="Off" red="60" green="100" blue="60">
      </color>
    </background_color>
  </widget>
```

# Description : Permet de calculer la moyenne de 4 valeurs et d'envoyer la moyenne dans un widget de type Text Update.
# Auteur : Olivier DELAHAYE 
# Date : 23/09/2022

# import des packages pour gérer les PVs et widgets dans phoebus
from org.csstudio.display.builder.runtime.script import PVUtil
from org.csstudio.display.builder.runtime.script import ScriptUtil
from org.csstudio.display.builder.runtime.script import ColorFontUtil

###############################################################################
# Le script commmence ici
################################################################################

#mywidget = ScriptUtil.findWidgetByName(widget, 'my widget name')
#RED = ColorFontUtil.RED
#widget.setPropertyValue("start_angle", value)
#widget.setPropertyValue("foreground_color", RED)
print("> calculerMoyenne.py")

startStop = PVUtil.getString(pvs[0])
print("   pv0="+str(pvs[0]))
print("   startStop=" + str(startStop))
if startStop == "start":
    print("   demarrer calcul moyenne")
    val1 = PVUtil.getDouble(pvs[1])
    val2 = PVUtil.getDouble(pvs[2])
    val3 = PVUtil.getDouble(pvs[3])
    val4 = PVUtil.getDouble(pvs[4])
    print("   val1=" + str(val1))
    print("   val2=" + str(val2))
    print("   val3=" + str(val3))
    print("   val4=" + str(val4))
    moy = (val1+val2+val3+val4) / 4
    print("   moy=" + str(moy))
    pvs[5].write(moy)
    startStop = "stop"
print("< calculerMoyenne.py")

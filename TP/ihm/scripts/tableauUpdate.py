# Description : Permet de mettre à jour un graphique
# Auteur : Olivier DELAHAYE 
# Date : 23/09/2022

# import des packages pour gérer les PVs et widgets dans phoebus
from org.csstudio.display.builder.runtime.script import PVUtil
from org.csstudio.display.builder.runtime.script import ScriptUtil
from org.csstudio.display.builder.runtime.script import ColorFontUtil

###############################################################################
# Le script commmence ici
################################################################################

print("> tableauUpdate.py")

tableData = []
for i in range(0, 4):   
    col0 = str(i+1)
    value = PVUtil.getDouble(pvs[i])
    print("    value = " + str(value))
    col1 = str(value)
    row = [col0, col1]
    tableData.append(row)
    print("   tableData=" + str(tableData))

widget.setValue(tableData)

print("< tableauUpdate.py")

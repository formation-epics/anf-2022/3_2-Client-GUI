# Description : Permet de mettre à jour un graphique
# Auteur : Olivier DELAHAYE 
# Date : 23/09/2022

# import des packages pour gérer les PVs et widgets dans phoebus
from org.csstudio.display.builder.runtime.script import PVUtil
from org.csstudio.display.builder.runtime.script import ScriptUtil
from org.csstudio.display.builder.runtime.script import ColorFontUtil

###############################################################################
# Le script commmence ici
################################################################################

#mywidget = ScriptUtil.findWidgetByName(widget, 'my widget name')
#RED = ColorFontUtil.RED
#widget.setPropertyValue("start_angle", value)
#widget.setPropertyValue("foreground_color", RED)
print("> graphUpdate.py")

valueList = []
for i in range(0, 4):
    pvValue = PVUtil.getDouble(pvs[i])
    valueList.append(pvValue)

print("   valueList=" + str(valueList))
pvs[4].write(valueList)

print("< graphUpdate.py")

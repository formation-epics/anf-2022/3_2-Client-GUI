# Description : Permet de changer la couleur d'un widget.
# Auteur : Olivier DELAHAYE 
# Date : 05/10/2022

# import des packages pour gérer les PVs et widgets dans phoebus
from org.csstudio.display.builder.runtime.script import PVUtil
from org.csstudio.display.builder.runtime.script import ScriptUtil
from org.csstudio.display.builder.runtime.script import ColorFontUtil

###############################################################################
# Le script commmence ici
################################################################################

print("> setColor.py")

startStop = PVUtil.getString(pvs[0])
print("   pv0="+str(pvs[0]))
print("   startStop=" + str(startStop))
if startStop == "start":
    mywidget = ScriptUtil.findWidgetByName(widget, 'Rectangle')
    mywidget.setPropertyValue("background_color", ColorFontUtil.RED)

print("< setColor.py")

# 3_2 Client GUI (Graphical User Interface)
Présentation sur les logiciels d'interface homme machine pour EPICS (Experimental Physics and Industrial Control System)<p>
The projet is public, you can download the presentation directly from GitLab with Download button.<br/>
From Linux, you can use the commands:
```
wget https://gitlab.com/formation-epics/anf-2022/3_2-Client-GUI/-/archive/main/3_2-Client-GUI-main.zip <br/>
unzip 3_2-Client-GUI-main.zip
```
To modifiy the slides, you need to be a member of the project and clone the project.